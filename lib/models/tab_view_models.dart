import 'package:flutter/material.dart';

class TabViewModel {
  final String tabName;
  final Widget widget;

  TabViewModel({
    this.tabName,
    this.widget,
  });
}
