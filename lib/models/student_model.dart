import 'package:flutter/foundation.dart';

class Student {
  String id;
  String firstName;
  String middleName;
  String lastName;
  String birthdate;
  String gender;
  String completeAddress;
  String mobileNumber;
  String alternateMobileNumber;
  String emailAdd;
  String facebookLink;
  String contactName;
  String relationship;
  String contactNumber;
  String alternateContactNumber;
  String dateEnrolled;
  String studentID;
  String year;
  String section;
  String image;

  Student({
    @required this.id,
    @required this.firstName,
    @required this.middleName,
    @required this.lastName,
    @required this.birthdate,
    @required this.gender,
    @required this.completeAddress,
    @required this.mobileNumber,
    @required this.alternateMobileNumber,
    @required this.emailAdd,
    @required this.facebookLink,
    @required this.contactName,
    @required this.relationship,
    @required this.contactNumber,
    @required this.alternateContactNumber,
    @required this.dateEnrolled,
    @required this.studentID,
    @required this.year,
    @required this.section,
    @required this.image,
});

  factory Student.fromJson(dynamic json) {
    return Student(
      id: json['id'] as String,
      firstName: json['firstName'] as String,
      middleName: json['middleName'] as String,
      lastName: json['lastName'] as String,
      birthdate: json['birthdate'] as String,
      gender: json['gender'] as String,
      completeAddress: json['completeAddress'] as String,
      mobileNumber: json['mobileNumber'] as String,
      alternateMobileNumber: json['alternateMobileNumber'] as String,
      emailAdd: json['emailAdd'] as String,
      facebookLink: json['facebookLink'] as String,
      contactName: json['contactName'] as String,
      relationship: json['relationship'] as String,
      contactNumber: json['contactNumber'] as String,
      alternateContactNumber: json['alternateContactNumber'] as String,
      dateEnrolled: json['dateEnrolled'] as String,
      studentID: json['studentID'] as String,
      year: json['year'] as String,
      section: json['section'] as String,
      image: json['image'] as String,
    );
  }
}