import 'package:flutter/material.dart';

class TableColumn {
  final String columnName;
  final int span;

  TableColumn({
    @required this.columnName,
    this.span,
  });
}

class TableRows {
  final Function onPressed;
  final List<Widget> cells;

  TableRows({
    this.onPressed,
    @required this.cells,
  });
}
