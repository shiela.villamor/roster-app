const String getListStudents = r'''
  query{
  allStudents{
    id
    firstName
    middleName
    lastName
    birthdate
    gender
    completeAddress
    mobileNumber
    alternateMobileNumber
    emailAdd
    facebookLink
    contactName
    relationship
    contactNumber
    alternateContactNumber
    dateEnrolled
    studentID
    year
    section
    image
  }
}
''';


const String createOneStudent = r'''
mutation createStudentMutation(
  $id: ID!,
  $firstName: String!,
  $middleName: String!,
  $lastName: String!,
  $birthdate: String!,
  $gender: String!,
  $completeAddress: String!,
  $mobileNumber: String!,
  $alternateMobileNumber: String,
  $emailAdd: String!,
  $facebookLink: String,
  $contactName: String!,
  $relationship: String!,
  $contactNumber: String!,
  $alternateContactNumber: String!,
  $dateEnrolled: String!,
  $studentID: String!,
  $year: String!,
  $section: String!,
  $image: String!
){
  createStudent(
  id: $id,
  firstName: $firstName,
  middleName: $middleName,
  lastName: $lastName,
  birthdate: $birthdate,
  gender: $gender,
  completeAddress: $completeAddress,
  mobileNumber: $mobileNumber,
  alternateMobileNumber: $alternateMobileNumber,
  emailAdd: $emailAdd,
  facebookLink: $facebookLink,
  contactName: $contactName,
  relationship: $relationship,
  contactNumber: $contactNumber,
  alternateContactNumber: $alternateContactNumber,
  dateEnrolled: $dateEnrolled,
  studentID: $studentID,
  year: $year,
  section: $section,
  image: $image
  ){
    id
    firstName
    middleName
    lastName
    birthdate
    gender
    completeAddress
    mobileNumber
    alternateMobileNumber
    emailAdd
    facebookLink
    contactName
    relationship
    contactNumber
    alternateContactNumber
    dateEnrolled
    studentID
    year
    section
    image
  }
}
''';


const String updateOneStudent = r'''
mutation updateStudentMutation(
  $id: ID!,
  $firstName: String!,
  $middleName: String!,
  $lastName: String!,
  $birthdate: String!,
  $gender: String!,
  $completeAddress: String!,
  $mobileNumber: String!,
  $alternateMobileNumber: String,
  $emailAdd: String!,
  $facebookLink: String,
  $contactName: String!,
  $relationship: String!,
  $contactNumber: String!,
  $alternateContactNumber: String!,
  $dateEnrolled: String!,
  $studentID: String!,
  $year: String!,
  $section: String!,
  $image: String!
){
  updateStudent(
  id: $id,
  firstName: $firstName,
  middleName: $middleName,
  lastName: $lastName,
  birthdate: $birthdate,
  gender: $gender,
  completeAddress: $completeAddress,
  mobileNumber: $mobileNumber,
  alternateMobileNumber: $alternateMobileNumber,
  emailAdd: $emailAdd,
  facebookLink: $facebookLink,
  contactName: $contactName,
  relationship: $relationship,
  contactNumber: $contactNumber,
  alternateContactNumber: $alternateContactNumber,
  dateEnrolled: $dateEnrolled,
  studentID: $studentID,
  year: $year,
  section: $section,
  image: $image
  ){
    id
    firstName
    middleName
    lastName
    birthdate
    gender
    completeAddress
    mobileNumber
    alternateMobileNumber
    emailAdd
    facebookLink
    contactName
    relationship
    contactNumber
    alternateContactNumber
    dateEnrolled
    studentID
    year
    section
    image
  }
}
''';


