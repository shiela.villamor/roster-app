import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rosterapp/graphql/graph_client.dart';
import 'package:rosterapp/graphql/query.dart';

final addStudentProvider = ChangeNotifierProvider<AddStudentProvider>((ref) {
  return AddStudentProvider();
});

final personalDateProvider = ChangeNotifierProvider((ref) => DateProvider());
final personalSubmitProvider = ChangeNotifierProvider((ref) => AddStudentProvider());
final dropDownProvider = ChangeNotifierProvider((ref) => DropdownProvider());

class AddStudentProvider extends ChangeNotifier {
  final GraphQLClient _graphQLClient = client.value;

  String gender;
  String _id;
  String studentID;
  String firstName;
  String middleName;
  String lastName;
  String mobileNumber;
  String alternateMobileNumber;
  String emailAdd;
  String facebookLink;
  String completeAddress;
  String contactName;
  String alternateContactNumber;
  String relationship;
  String contactNumber;
  String image;
  String year;
  String section;
  String formType;
  bool _isSubmitted = false;
  bool isLoading = true;
  bool get isSubmitted => _isSubmitted;

  String get id => _id;

  //Date Fields
  String birthdate;
  DateTime dateEnrolled;

  void setFormType(String formTypeValue) {
    formType = formTypeValue;
    notifyListeners();
  }

  void setLoader(bool loaderValue) {
    isLoading = loaderValue;
    notifyListeners();
  }

  void setGender(String genderValue) {
    gender = genderValue;
    notifyListeners();
  }

  void setId(String value){
    _id = value;
    notifyListeners();
  }

  void setImage(String imageValue){
    image = imageValue;
    notifyListeners();
  }

  void onSubmit(bool value) {
    _isSubmitted = value;
    notifyListeners();
  }

  //Text Fields
  void setStudentID(String studentIDValue) {
    studentID = studentIDValue;
    notifyListeners();
  }

  void setFirstName(String firstNameValue) {
    firstName = firstNameValue;
    notifyListeners();
  }

  void setMiddleName(String middleNameValue) {
    middleName = middleNameValue;
    notifyListeners();
  }

  void setLastName(String lastNameValue) {
    lastName = lastNameValue;
    notifyListeners();
  }

  void setMobileNumber(String mobileNumberValue) {
    mobileNumber = mobileNumberValue;
    notifyListeners();
  }

  void setPersonalEmail(String personalEmailValue) {
    emailAdd = personalEmailValue;
    notifyListeners();
  }

  void setFacebookProfile(String facebookProfileValue) {
    facebookLink = facebookProfileValue;
    notifyListeners();
  }

  void setPresentLineAddress(String presentLineAddressValue) {
    completeAddress = presentLineAddressValue;
    notifyListeners();
  }

  void setContactName(String contactNameValue) {
    contactName = contactNameValue;
    notifyListeners();
  }

  void setRelationship(String relationshipValue) {
    relationship = relationshipValue;
    notifyListeners();
  }

  void setAlternateContactNumber(String alternateNumberValue) {
    alternateContactNumber = alternateNumberValue;
    notifyListeners();
  }

  void setAlternateMobileNumber(String alternateNumberValue) {
    alternateMobileNumber = alternateNumberValue;
    notifyListeners();
  }

  void setContactNumber(String contactNumberValue) {
    contactNumber = contactNumberValue;
    notifyListeners();
  }

  void setYear(String yearValue) {
    year = yearValue;
    notifyListeners();
  }

  void setSection(String sectionValue) {
    section = sectionValue;
    notifyListeners();
  }


  void resetOnSubmit() {
    _isSubmitted = false;
    notifyListeners();
  }

  void resetValues() {
    _id = null;
    firstName = null;
    middleName = null;
    lastName = null;
    birthdate = null;
    gender = null;
    completeAddress = null;
    mobileNumber = null;
    alternateMobileNumber = null;
    emailAdd = null;
    facebookLink = null;
    contactName = null;
    relationship = null;
    contactNumber = null;
    alternateContactNumber = null;
    dateEnrolled = null;
    studentID = null;
    year = null;
    section = null;
    image = null;
  }

  Future editStudent() async {
    final QueryResult result = await _graphQLClient
        .mutate(MutationOptions(
        document: gql(updateOneStudent),
        variables: {
          'id': id,
          'firstName': firstName,
          'middleName': middleName,
          'lastName': lastName,
          'birthdate': birthdate,
          'gender': gender.toString(),
          'completeAddress': completeAddress,
          'mobileNumber': mobileNumber,
          'alternateMobileNumber': alternateMobileNumber,
          'emailAdd': emailAdd,
          'facebookLink': facebookLink,
          'contactName': contactName,
          'relationship': relationship,
          'contactNumber': contactNumber,
          'alternateContactNumber': '09123456789',
          'dateEnrolled': 'April 13, 2021',
          'studentID': studentID,
          'year': year.toString(),
          'section': section.toString(),
          'image': image
        }
    ));

    if(result.hasException){
      print('${result.exception}');
    }
    notifyListeners();
  }



  Future createStudent() async {
    final QueryResult result = await _graphQLClient
        .mutate(MutationOptions(
      document: gql(createOneStudent),
      variables: {
        'id': _id,
        'firstName': firstName,
        'middleName': middleName,
        'lastName': lastName,
        'birthdate': birthdate,
        'gender': gender.toString(),
        'completeAddress': completeAddress,
        'mobileNumber': mobileNumber,
        'alternateMobileNumber': alternateMobileNumber,
        'emailAdd': emailAdd,
        'facebookLink': facebookLink,
        'contactName': contactName,
        'relationship': relationship,
        'contactNumber': contactNumber,
        'alternateContactNumber': '09123456789',
        'dateEnrolled': 'April 13, 2021',
        'studentID': studentID,
        'year': year.toString(),
        'section': section.toString(),
        'image': image
      }
    ));

    if(result.hasException){
      print('${result.exception}');
    }
    notifyListeners();
  }


  //Date Fields
  void setBirthDate(String birthDateValue) {
    birthdate = birthDateValue;
    notifyListeners();
  }

  void setDateEnrolled(DateTime dateEnrolledValue) {
    dateEnrolled = dateEnrolledValue;
    notifyListeners();
  }
}


class DateProvider extends ChangeNotifier {
  DateTime _birthday;
  DateTime _dateEnrolled;

  DateTime get birthday => _birthday;
  DateTime get dateEnrolled => _dateEnrolled;


  void setBirthday(DateTime dateInput) {
    _birthday = dateInput;
    notifyListeners();
  }

  void setEnrolmentDate(DateTime dateInput) {
    _dateEnrolled = dateInput;
    notifyListeners();
  }

  void resetValues() {
    _birthday = null;
    _dateEnrolled = null;
  }
}


class DropdownProvider extends ChangeNotifier {
  String gender;
  String year;
  String section;

  void setGender(String genderValue) {
    gender = genderValue;
    notifyListeners();
  }

  void setYear(String yearValue) {
    year = yearValue;
    notifyListeners();
  }

  void setSection(String sectionValue) {
    section = sectionValue;
    notifyListeners();
  }

  void resetValues() {
    gender = null;
    year = null;
  }
}