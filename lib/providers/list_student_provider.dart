import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rosterapp/graphql/graph_client.dart';
import 'package:rosterapp/graphql/query.dart';
import 'package:rosterapp/models/student_model.dart';


final listStudentProvider = ChangeNotifierProvider<ListStudentProvider>((ref) {
  return ListStudentProvider();
});

class ListStudentProvider extends ChangeNotifier {
  List<Student> _listOfStudents = [];

  List<Student> get listOfStudents => _listOfStudents;

  final GraphQLClient _graphqlClient = client.value;

  Future getListOfStudents() async {
    final QueryResult result =
    await _graphqlClient.query(QueryOptions(
        document: gql(getListStudents),
    fetchPolicy: FetchPolicy.noCache));

    if (result.hasException) {
      print('No data');
      return Text('No Data');
    } else {
      final List<dynamic> students = result.data['allStudents'] as List<dynamic>;
      _listOfStudents = students
          .map((item) => Student.fromJson(item))
          .toList();
    }
    notifyListeners();
  }
}