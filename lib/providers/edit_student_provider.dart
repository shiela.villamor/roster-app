import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rosterapp/graphql/graph_client.dart';
import 'package:rosterapp/graphql/query.dart';
import 'package:rosterapp/models/student_model.dart';

final editStudentProvider = ChangeNotifierProvider<EditStudentProvider>((ref) {
  return EditStudentProvider();
});

final personalDateProvider = ChangeNotifierProvider((ref) => DateProvider());
final personalSubmitProvider = ChangeNotifierProvider((ref) => EditStudentProvider());
final dropDownProvider = ChangeNotifierProvider((ref) => DropdownProvider());

class EditStudentProvider extends ChangeNotifier {
  String gender;
  String id;
  String studentID;
  String firstName;
  String middleName;
  String lastName;
  String mobileNumber;
  String alternateMobileNumber;
  String emailAdd;
  String facebookLink;
  String completeAddress;
  String contactName;
  String alternateContactNumber;
  String relationship;
  String contactNumber;
  String image;
  String year;
  String section;
  bool _isSubmitted = false;
  bool get isSubmitted => _isSubmitted;
  Student _student;

  Student get student => _student;


  //Date Fields
  String birthdate;
  DateTime dateEnrolled;

  void setStudent(Student value){
    _student = value;
    notifyListeners();
  }

  void setGender(String genderValue) {
    gender = genderValue;
  }

  void setId(String value){
    id = value;
  }

  void setImage(String imageValue){
    image = imageValue;
  }

  void onSubmit(bool value) {
    _isSubmitted = value;
    if(value) {}
  }

  //Text Fields
  void setStudentID(String studentIDValue) {
    studentID = studentIDValue;
  }

  void setFirstName(String firstNameValue) {
    firstName = firstNameValue;

  }

  void setMiddleName(String middleNameValue) {
    middleName = middleNameValue;
  }

  void setLastName(String lastNameValue) {
    lastName = lastNameValue;
  }

  void setMobileNumber(String mobileNumberValue) {
    mobileNumber = mobileNumberValue;
  }

  void setPersonalEmail(String personalEmailValue) {
    emailAdd = personalEmailValue;
  }

  void setFacebookProfile(String facebookProfileValue) {
    facebookLink = facebookProfileValue;
  }

  void setPresentLineAddress(String presentLineAddressValue) {
    completeAddress = presentLineAddressValue;
  }

  void setContactName(String contactNameValue) {
    contactName = contactNameValue;
  }

  void setRelationship(String relationshipValue) {
    relationship = relationshipValue;
  }

  void setAlternateContactNumber(String alternateNumberValue) {
    alternateContactNumber = alternateNumberValue;
  }

  void setAlternateMobileNumber(String alternateNumberValue) {
    alternateMobileNumber = alternateNumberValue;
  }

  void setContactNumber(String contactNumberValue) {
    contactNumber = contactNumberValue;
  }

  void setYear(String yearValue) {
    year = yearValue;
  }

  void setSection(String sectionValue) {
    section = sectionValue;
  }


  void resetOnSubmit() {
    _isSubmitted = false;
    notifyListeners();
  }

  void resetValues() {
    id = null;
    firstName = null;
    middleName = null;
    lastName = null;
    birthdate = null;
    gender = null;
    completeAddress = null;
    mobileNumber = null;
    alternateMobileNumber = null;
    emailAdd = null;
    facebookLink = null;
    contactName = null;
    relationship = null;
    contactNumber = null;
    alternateContactNumber = null;
    dateEnrolled = null;
    studentID = null;
    year = null;
    section = null;
    image = null;
  }

  final GraphQLClient _graphQLClient = client.value;

  Future editStudent() async {
    final QueryResult result = await _graphQLClient
        .mutate(MutationOptions(
        document: gql(updateOneStudent),
        variables: {
          'id': id,
          'firstName': firstName,
          'middleName': middleName,
          'lastName': lastName,
          'birthdate': birthdate,
          'gender': gender.toString(),
          'completeAddress': completeAddress,
          'mobileNumber': mobileNumber,
          'alternateMobileNumber': alternateMobileNumber,
          'emailAdd': emailAdd,
          'facebookLink': facebookLink,
          'contactName': contactName,
          'relationship': relationship,
          'contactNumber': contactNumber,
          'alternateContactNumber': '09123456789',
          'dateEnrolled': 'April 13, 2021',
          'studentID': studentID,
          'year': year.toString(),
          'section': section.toString(),
          'image': image
        }
    ));

    if(result.hasException){
      print('${result.exception}');
    }
    notifyListeners();
  }


  //Date Fields
  void setBirthDate(String birthDateValue) {
    birthdate = birthDateValue;
  }

  void setDateEnrolled(DateTime dateEnrolledValue) {
    dateEnrolled = dateEnrolledValue;
    notifyListeners();
  }
}


class DateProvider extends ChangeNotifier {
  DateTime _birthday;
  DateTime _dateEnrolled;

  DateTime get birthday => _birthday;
  DateTime get dateEnrolled => _dateEnrolled;


  void setBirthday(DateTime dateInput) {
    _birthday = dateInput;
    notifyListeners();
  }

  void setEnrolmentDate(DateTime dateInput) {
    _dateEnrolled = dateInput;
    notifyListeners();
  }

  void resetValues() {
    _birthday = null;
    _dateEnrolled = null;
  }
}


class DropdownProvider extends ChangeNotifier {
  String gender;
  String year;
  String section;

  void setGender(String genderValue) {
    gender = genderValue;
    notifyListeners();
  }

  void setYear(String yearValue) {
    year = yearValue;
    notifyListeners();
  }

  void setSection(String sectionValue) {
    section = sectionValue;
    notifyListeners();
  }

  void resetValues() {
    gender = null;
    year = null;
  }
}