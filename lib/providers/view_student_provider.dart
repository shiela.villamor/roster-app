import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';

final viewProfileProvider = ChangeNotifierProvider<ViewProfileProvider>((ref) => ViewProfileProvider());

class ViewProfileProvider extends ChangeNotifier {
  Student _student;

  Student get student => _student;

  void setStudent(Student value) {
    _student = value;
    notifyListeners();
  }
}
