final result = {
  "data" : {
    "allStudents": [
      {
      "id": "1",
      "firstName": "Shiela Mae",
      "middleName": "Villamor",
      "lastName": "Kim",
      "birthdate": "December 4, 1998",
      "gender": "Female",
      "completeAddress": "Purok 1, Poblacion 2, Mabini, Bohol",
      "mobileNumber": "09260172619",
      "alternateMobileNumber": "09508829874",
      "emailAdd": "villamorshii@gmail.com",
      "facebookLink": "facebook.com/sheenim",
      "contactName": "Anita Villamor",
      "relationship": "Mother",
      "contactNumber": "09950392630",
      "alternateContactNumber": "09361173788",
      "dateEnrolled": "June 13, 2015",
      "studentID": "SID-813314",
      "year": "Freshman",
      "section": "St. Augustine",
      "image": "https://i.pinimg.com/736x/28/20/d5/2820d5bd47095126a5dd1b83ce7e1f33.jpg"
      },
      {
        "id": "2",
        "firstName": "Kyzelle Margaux",
        "middleName": "Suaner",
        "lastName": "Alvarez",
        "birthdate": "December 4, 1998",
        "gender": "Female",
        "completeAddress": "Purok 1, Poblacion 2, Mabini, Bohol",
        "mobileNumber": "09260172619",
        "alternateMobileNumber": "09508829874",
        "emailAdd": "kyzellemargaux@gmail.com",
        "facebookLink": "facebook.com/sheenim",
        "contactName": "Kryzelle Margarette Suaner",
        "relationship": "Mother",
        "contactNumber": "09950392630",
        "alternateContactNumber": "09361173788",
        "dateEnrolled": "June 15, 2017",
        "studentID": "SID-813314",
        "year": "Freshman",
        "section": "St. Thomas",
        "image": "https://i.mydramalist.com/qlep0_5f.jpg"
      }
    ]
  }
};




/*
class StudentList {
  String studentName;
  String studentID;
  String studentYear;

  StudentList(this.studentName, this.studentID, this.studentYear);
  static List<StudentList> getStudentList() {
    return <StudentList>[
      StudentList('Kim Hanbin', 'SID-881123', 'Freshman'),
      StudentList('Kim Jiwon', 'SID-881123', 'Freshman'),
      StudentList('Kim Donghyuk', 'SID-881123', 'Sophomore'),
      StudentList('Kim Jinhwan', 'SID-881123', 'Sophomore'),
      StudentList('Song Yunhyeong', 'SID-881123', 'Senior'),
      StudentList('Koo Junhoe', 'SID-881123', 'Freshman'),
      StudentList('Park Seojoon', 'SID-881123', 'Junior'),
      StudentList('Woo Dohwan', 'SID-881123', 'Junior'),
      StudentList('Lee Minho', 'SID-881123', 'Junior'),
      StudentList('Nam Joohyuk', 'SID-881123', 'Junior'),
      StudentList('Kim Seonho', 'SID-881123', 'Junior'),

    ];
  }
}*/
