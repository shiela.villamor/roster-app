// export '';

import 'package:flutter/cupertino.dart';

const double DesktopSize = 900;
const double MobileSize = 600;

const primaryColor = Color(0xff0ab0f1);
const secondaryColor = Color(0xffdadada);
