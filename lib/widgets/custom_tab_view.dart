
import 'package:flutter/material.dart';
import 'package:rosterapp/models/tab_view_models.dart';

class CustomTabView extends StatefulWidget {
  final List<TabViewModel> tabData;

  const CustomTabView({
    Key key,
    @required this.tabData,
  }) : super(key: key);

  @override
  CustomTabViewState createState() => CustomTabViewState();
}

class CustomTabViewState extends State<CustomTabView>
    with SingleTickerProviderStateMixin, RestorationMixin {
  TabController _tabController;

  final RestorableInt tabIndex = RestorableInt(0);

  @override
  String get restorationId => 'tab_scrollable';

  @override
  void restoreState(RestorationBucket oldBucket, bool initialRestore) {
    registerForRestoration(tabIndex, 'tab_index');
    _tabController.index = tabIndex.value;
  }

  @override
  void initState() {
    _tabController = TabController(
      length: widget.tabData.length,
      vsync: this,
    );
    _tabController.addListener(() {
      setState(() {
        tabIndex.value = _tabController.index;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.transparent;
      }
      return Colors.transparent;
    }

    return DefaultTabController(
      length: widget.tabData.length,
      child: Container(
        color: Color(0xffe97878),
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 50.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // Tab Buttons
            TabBar(
              physics: NeverScrollableScrollPhysics(),
              labelPadding: EdgeInsets.all(0),
              overlayColor: MaterialStateProperty.resolveWith(getColor),
              controller: _tabController,
              isScrollable: true,
              indicatorColor: Colors.transparent,
              indicatorWeight: 0.0001,
              tabs: _tabItem(
                widget.tabData,
                _tabController.index,
                context,
              ),
            ),

            // Body of the Tab
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width - 40,
                height: double.infinity,
                decoration: BoxDecoration(
                  color: Color(0xffdadada),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                  ),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0xffF4F1F1),
                    borderRadius: BorderRadius.only(
                      topLeft: _tabController.index == 0
                          ? Radius.circular(0)
                          : Radius.circular(10),
                      topRight: Radius.circular(0),
                      bottomRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xff000000).withOpacity(0.2),
                        spreadRadius: -10,
                        blurRadius: 34,
                        offset: Offset(0, 31), //X, Y
                      ),
                    ],
                  ),
                  child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: widget.tabData.map((res) => res.widget).toList(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _tabItem(
    List<TabViewModel> data,
    int tabIndex,
    BuildContext ctx,
  ) {
    final screenWidth = MediaQuery.of(context).size.width;
    final isDesktop = screenWidth > 900;

    return data
        .asMap()
        .entries
        .map((entry) => SizedBox(
              height: isDesktop ? 60 : 50,
              width:
                  isDesktop ? 189 : (screenWidth - 40) / _tabController.length,
              child: Tab(
                child: Container(
                  margin: isDesktop ? EdgeInsets.only(right: 1.0) : null,
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        topLeft: Radius.circular(10),
                      ),
                    ),
                    color: tabIndex == entry.key
                        ? Color(0xffffffff)
                        : Color(0xffdadada),
                  ),
                  child: Center(
                    child: Text(
                      entry.value.tabName,
                      style: TextStyle(
                        fontSize: 15,
                          fontFamily: 'Amaranth-Regular',
                        color: tabIndex == entry.key
                            ? Color(0xff903749)
                            : Color(0xffffffff),
                      ),
                    ),
                  ),
                ),
              ),
            ))
        .toList();
  }
}
