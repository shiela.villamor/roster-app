import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/models.dart';
import 'package:rosterapp/pages/student_profile.dart';
import 'package:rosterapp/providers/edit_student_provider.dart';
import 'package:rosterapp/providers/list_student_provider.dart';
import 'package:rosterapp/widgets/custom_table.dart';
import 'package:rosterapp/widgets/page-widget/student_forms.dart';


class ListPageWidget extends StatefulWidget {
  @override
  _ListPageWidgetState createState() => _ListPageWidgetState();
}

class _ListPageWidgetState extends State<ListPageWidget> {
  /*final ListStudentProvider studentProvider = ListStudentProvider();
  List<Student> studentItem = [];

  void getData() async {
   // studentProvider.getListOfStudents();
    setState(() {
      studentItem = studentProvider.listOfStudents;
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }*/


  @override
  Widget build(BuildContext context) {
    final List<TableColumn> columns = [
      TableColumn(columnName: 'Students', span: 5),
      TableColumn(columnName: 'Student ID', span: 3),
      TableColumn(columnName: 'Year', span: 3),
      TableColumn(columnName: '', span: 1)
    ];
    return Consumer(builder: (context, watch, child){
      final editProvider = watch(editStudentProvider);
      final studentProvider = watch(listStudentProvider);
      studentProvider.getListOfStudents();
      return Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
        children: [
          CustomTable(
            dataRowHeight: 48,
            columnHeaderVisible: false,
            //noDataFoundMessage: 'No results found.',
            columns: columns,
            rows: studentProvider.listOfStudents.isNotEmpty
                ? renderDataRowItem(
                studentProvider.listOfStudents, editProvider
              ) : [],
           )
        ],
      ),
      );
    });

  }

  List<TableRows> renderDataRowItem(_studentList, editProvider) {
    final students = _studentList
        .map<TableRows>(
        (result) => TableRows(
          onPressed: () {
            Navigator.push(context, CupertinoPageRoute(builder: (context) => StudentProfile(result)));
          },
          cells: [
            Row(
              children: [
                Flexible(child: Text(
                  '${result.firstName} ${result.lastName}',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 12, fontFamily: 'Amaranth-Regular'
                  ),
                ))
              ],
            ),
            Text(
              result.studentID.toString(),
              style: TextStyle(
                  fontSize: 12,
                  fontFamily: 'Amaranth-Regular'
              ),
            ),
            Text(
              result.year.toString(),
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: 12, fontFamily: 'Amaranth-Regular'
              ),
            ),
            _ellipsisButton(result, editProvider)
          ]
        )
    ).toList();

    return students;
  }

  Widget _ellipsisButton(result, editProvider){
      return PopupMenuButton<String>(
        offset: Offset(-6, MediaQuery.of(context).size.height <= 667 ? 15 : -10),
        icon: Icon(
          Icons.more_vert,
          size: 17,
          color: Color(0xff8B7D7B),
        ),
        padding: EdgeInsets.zero,
        onSelected: (value) async {
          if(value == 'View') {
            Navigator.push(context, CupertinoPageRoute(builder: (context) => StudentProfile(result)));
          }
          if(value == 'Edit'){
            //provider.setStudent(result);
            Navigator.push(context, CupertinoPageRoute(builder: (context) => StudentForms(result)));
          }
        },
        itemBuilder: (context) => <PopupMenuItem<String>>[
          PopupMenuItem<String>(
            height: 40,
            value: 'View',
            child: Text(
              'View',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400, fontFamily: 'Amaranth-Regular',
                color: Color(0xff222a34),
              ),
            ),
          ),
          PopupMenuItem<String>(
            height: 40,
            value: 'Edit',
            child: Text(
              'Edit',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400, fontFamily: 'Amaranth-Regular',
                color: Color(0xff222a34),
              ),
            ),
          ),
        ],
        tooltip: 'More',
      );

  }
}
