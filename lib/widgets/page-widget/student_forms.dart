import 'package:flutter/material.dart';
import 'package:rosterapp/models/student_model.dart';
import 'package:rosterapp/pages/add_student.dart';
import 'package:rosterapp/providers/add_student_provider.dart';
import 'package:rosterapp/providers/list_student_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


    class StudentForms extends StatefulWidget {
      final Student studentResult;

      StudentForms(this.studentResult, {Key key})
          : super(key: key);

      @override
      _StudentFormsState createState() => _StudentFormsState();
    }

    class _StudentFormsState extends State<StudentForms> {
      final formTypeProvider = AddStudentProvider();
      final getStudentsProvider = ListStudentProvider();
      final key = GlobalKey();
      bool isLoading = false;

      String firstName;
      String lastName;

      @override
  void initState() {
    if(widget.studentResult.id != null) {
      setState(() {
        isLoading = true;
        getData();
      });
    }
    super.initState();
  }

    Future getData() async {
        setState(() {
          isLoading = false;
        });

        context.read(addStudentProvider).setFormType('EDIT');
        context.read(addStudentProvider).setId(widget.studentResult.id);
        context.read(addStudentProvider).setImage(widget.studentResult.image);
        context.read(addStudentProvider).setFirstName(widget.studentResult.firstName);
        context.read(addStudentProvider).setMiddleName(widget.studentResult.middleName);
        context.read(addStudentProvider).setLastName(widget.studentResult.lastName);
        context.read(addStudentProvider).setBirthDate(widget.studentResult.birthdate);
        context.read(addStudentProvider).setGender(widget.studentResult.gender);
        context.read(addStudentProvider).setPresentLineAddress(widget.studentResult.completeAddress);
        context.read(addStudentProvider).setMobileNumber(widget.studentResult.mobileNumber);
        context.read(addStudentProvider).setAlternateMobileNumber(widget.studentResult.mobileNumber);
        context.read(addStudentProvider).setPersonalEmail(widget.studentResult.emailAdd);
        context.read(addStudentProvider).setFacebookProfile(widget.studentResult.facebookLink);
        context.read(addStudentProvider).setContactName(widget.studentResult.contactName);
        context.read(addStudentProvider).setRelationship(widget.studentResult.relationship);
        context.read(addStudentProvider).setContactNumber(widget.studentResult.contactNumber);
        context.read(addStudentProvider).setStudentID(widget.studentResult.studentID);
        context.read(addStudentProvider).setYear(widget.studentResult.year);
        context.read(addStudentProvider).setSection(widget.studentResult.section);
    }

      @override
      Widget build(BuildContext context) {
        return AddStudent();
      }
    }
