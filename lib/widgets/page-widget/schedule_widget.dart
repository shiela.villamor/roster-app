import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';


class SchedulePageWidget extends ConsumerWidget {
  final List<Student> studentList;

  SchedulePageWidget(this.studentList, {Key key})
    : super(key: key);
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    String day = '';
    String time = '';
    return Scaffold(
      //backgroundColor: Color(0xffD55858),
      body: ListView.builder(
        itemCount: studentList.length,
        itemBuilder: (BuildContext context, int index) {
          if (studentList[index].year == 'Freshman' && studentList[index].gender == 'Male'){
            day = 'Monday';
            time = '8:30AM-11:30AM';
          }
          else if (studentList[index].year == 'Freshman' && studentList[index].gender == 'Female'){
            day = 'Monday';
            time = '1:30PM-4:30PM';
          }
          else if(studentList[index].year == 'Sophomore' && studentList[index].gender == 'Male'){
            day = 'Tuesday';
            time = '8:30AM-11:30AM';
          }
          else if(studentList[index].year == 'Sophomore' && studentList[index].gender == 'Female'){
            day = 'Tuesday';
            time = '1:30PM-4:30PM';
          }
          else if(studentList[index].year == 'Junior' && studentList[index].gender == 'Male'){
            day = 'Wednesday';
            time = '8:30AM-11:30AM';
          }
          else if(studentList[index].year == 'Junior' && studentList[index].gender == 'Female'){
            day = 'Wednesday';
            time = '1:30PM-4:30PM';
          }
          else if(studentList[index].year == 'Senior' && studentList[index].gender == 'Male'){
            day = 'Thursday';
            time = '8:30AM-11:30AM';
          }
          else if(studentList[index].year == 'Senior' && studentList[index].gender == 'Female'){
            day = 'Thursday';
            time = '1:30PM-4:30PM';
          }
          else{
            day = 'Friday';
            time = '8:30AM-11:30AM';
          }
          return Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7)
            ),
            color: Color(0xffA96E6E),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  contentPadding: EdgeInsets.all(5.0),
                  leading: Image.asset('assets/images/time.png'),
                  title: Text(
                    '${studentList[index].lastName}, ${studentList[index].firstName[0]}. ${studentList[index].middleName[0]}.',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Amaranth-Regular',
                        fontSize: 15.0
                    ),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                      studentList[index].section,
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'Amaranth-Regular'
                        ),
                    ),
                      Text(
                        studentList[index].year,
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Amaranth-Regular'
                        ),
                      )
            ]
                  ),

                  trailing: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Schedule: $day',
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Amaranth-Regular'
                        ),
                      ),
                      Text(
                        'Time: $time',
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Amaranth-Regular'
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        },

      )
    );
  }
}
