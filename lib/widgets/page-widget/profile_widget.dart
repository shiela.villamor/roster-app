
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';

import '../fields-widgets/expansion_details.dart';

class ProfileWidget extends StatefulWidget {
  final Student studentResult;

  ProfileWidget(this.studentResult, {Key key})
      : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget>
  with SingleTickerProviderStateMixin, RestorationMixin {

  TabController _tabController;
  final RestorableInt tabIndex = RestorableInt(0);

  @override
  String get restorationId => 'tab_scrollable_demo';

  @override
  void restoreState(RestorationBucket oldBucket, bool initialRestore) {
    registerForRestoration(tabIndex, 'tab_index');
    _tabController.index = tabIndex.value;
  }

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      setState(() {
        tabIndex.value = _tabController.index;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child){
      return Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          color: Color(0xffe97878),
          padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 50.0),
          child: Center(
            child: Column(
              children: [
                _topData(widget.studentResult),
                SizedBox(height: 20.0),
                ExpansionDetails(widget.studentResult),
              ],
            ),
          ),
        ),
      );
    });
  }


  Widget _topData(studentResult) {
      return Container(
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 0.0, 20.0),
        decoration: BoxDecoration(
            color: Color(0xffdadada), borderRadius: BorderRadius.circular(5.0)),
        child: Column(
          children: [
            Row(
              children: [
                SizedBox(
                  width: 50,
                  height: 50,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image(
                      image: NetworkImage(
                         studentResult.image
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${studentResult.firstName} ${studentResult.middleName[0]}. ${studentResult.lastName}',
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w500, fontFamily: 'Amaranth-Regular'
                      ),
                    ),
                    Text(
                      '${studentResult.studentID}  |  ${studentResult.year}',
                      style: TextStyle(
                          fontSize: 12.5,
                          fontWeight: FontWeight.w400,
                          color: Color(0xffA96E6E), fontFamily: 'Amaranth-Regular'
                      ),
                    )
                  ],
                )
              ],
            ),
          ],
        ),
      );
  }


}
