import 'package:flutter/material.dart';

class CustomRowLabel extends StatelessWidget {
  final String label;
  final String input;

  const CustomRowLabel({
    Key key,
    this.label,
    this.input,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                  width: MediaQuery.of(context).size.width * 0.30,
                  child: Text(
                    label,
                    style: TextStyle(
                        color: Color(0xFF9B9B9B),
                        fontSize: 13.0,
                        fontFamily: 'Amaranth-Regular'),
                    maxLines: 2,
                  )),
              SizedBox(width: 10.0),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.45,
                child: Text(
                  input,
                  style: TextStyle(
                      color: Color(0xFF222A34),
                      fontSize: 13.0,
                      fontFamily: 'Amaranth-Regular'),
                  maxLines: 3,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          )
        ],
      );
}
