
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/providers/edit_student_provider.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/dropdown/select_gender.dart';

// ignore: camel_case_types
class EditGenderDropdown extends StatelessWidget {
  const EditGenderDropdown({
    Key key,
    @required this.dropdownProvider,
    @required this.submitProvider,
  }) : super(key: key);

  final EditStudentProvider dropdownProvider;
  final EditStudentProvider submitProvider;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: <Widget>[
            Flexible(
              child: GestureDetector(
                onTap: () async {
                  final String _gender = await Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => SelectGender()));
                  dropdownProvider.setGender(_gender);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 24, right: 24, bottom: 12),
                  padding: EdgeInsets.only(left: 10, right: 20),
                  height: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(
                        color: dropdownProvider.gender == null &&
                            submitProvider.isSubmitted
                            ? Color(0xffeb2121)
                            : Color(0xFF8E8E8E)),
                  ),
                  child: Row(children: [
                    Text(dropdownProvider.gender ?? 'Gender',
                        textAlign: TextAlign.left,
                        style: dropdownProvider.gender == null ?
                        TextStyle(
                          color: Color(0xff8E8E8E),
                          fontSize: 11.0,
                            fontFamily: 'Amaranth-Regular'
                        ) :
                        TextStyle(
                            fontSize: 15, fontFamily: 'Amaranth-Regular'
                        )
                    ),
                    Spacer(),
                    SizedBox(
                      width: 25,
                      height: 50,
                      child: TextButton(
                        onPressed: () async {
                          final String _gender = await Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => SelectGender()));
                          dropdownProvider.setGender(_gender);
                        },
                        child: Icon(
                          Icons.arrow_drop_down,
                          color: Color(0xffA96E6E),
                          size: 35,
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          ],
        ),
        Consumer(builder: (context, watch, child) {
          final provider = watch(personalSubmitProvider);
          final choiceProvider = watch(dropDownProvider);
          if (provider.isSubmitted && choiceProvider.gender == null) {
            return Container(
              margin: EdgeInsets.only(left: 36, bottom: 10),
              child: Text(
                'Gender is required',
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xffEB2121),
                ),
              ),
            );
          } else {
            return SizedBox();
          }
        })
      ],
    );
  }
}
