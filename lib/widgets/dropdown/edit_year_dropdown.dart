
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/dropdown/select_year.dart';
import 'package:rosterapp/providers/edit_student_provider.dart';


class EditYearDropdown extends StatelessWidget {
  const EditYearDropdown({
    Key key,
    @required this.dropdownProvider,
    @required this.submitProvider,
  }) : super(key: key);

  final EditStudentProvider dropdownProvider;
  final EditStudentProvider submitProvider;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: <Widget>[
            Flexible(
              child: GestureDetector(
                onTap: () async {
                  final String _year = await Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => SelectYear()));
                  dropdownProvider.setYear(_year);
                },
                child: Container(
                  margin: EdgeInsets.only(left: 24, right: 24, bottom: 12),
                  padding: EdgeInsets.only(left: 10, right: 20),
                  height: 55,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(
                        color: dropdownProvider.year == null &&
                            submitProvider.isSubmitted
                            ? Color(0xffeb2121)
                            : Color(0xFF8E8E8E)),
                  ),
                  child: Row(children: [
                    Text(dropdownProvider.year ?? 'Year',
                        textAlign: TextAlign.left,
                        style: dropdownProvider.year == null ?
                        TextStyle(
                            color: Color(0xff8E8E8E),
                            fontSize: 11.0, fontFamily: 'Amaranth-Regular'
                        ) :
                        TextStyle(
                            fontSize: 15, fontFamily: 'Amaranth-Regular'
                        )
                    ),
                    Spacer(),
                    SizedBox(
                      width: 25,
                      height: 50,
                      child: TextButton(
                        onPressed: () async {
                          final String _year = await Navigator.of(context).push(
                              MaterialPageRoute(builder: (context) => SelectYear()));
                          dropdownProvider.setYear(_year);
                        },
                        child: Icon(
                          Icons.arrow_drop_down,
                          color: Color(0xffA96E6E),
                          size: 35,
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          ],
        ),
        Consumer(builder: (context, watch, child) {
          final provider = watch(personalSubmitProvider);
          final choiceProvider = watch(dropDownProvider);
          if (provider.isSubmitted && choiceProvider.year == null) {
            return Container(
              margin: EdgeInsets.only(left: 36, bottom: 10),
              child: Text(
                'Year is required',
                style: TextStyle(
                  fontSize: 12,
                  color: Color(0xffEB2121),
                ),
              ),
            );
          } else {
            return SizedBox();
          }
        })
      ],
    );
  }
}
