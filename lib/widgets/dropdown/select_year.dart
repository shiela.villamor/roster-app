import 'package:flutter/material.dart';

class SelectYear extends StatefulWidget {
  @override
  _SelectGender createState() => _SelectGender();
}

class _SelectGender extends State<SelectYear> {
  final List<String> entries = <String>['Freshman', 'Sophomore', 'Junior', 'Senior'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.close),
            iconSize: 25,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            'Year',
            style: TextStyle(
                fontSize: 15.0, fontFamily: 'Amaranth-Regular'
            ),
          ),
          elevation: 0,
          backgroundColor: Color(0xff903749),
        ),
        body: ListView.builder(
            padding: const EdgeInsets.all(20),
            itemCount: entries.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                  onTap: () {
                    Navigator.pop(
                      context,
                      entries[index],
                    );
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.only(left: 10),
                    height: MediaQuery.of(context).size.height * .048,
                    child: Text(entries[index],
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Color(0xff222A34),
                            fontSize: 14.0, fontFamily: 'Amaranth-Regular'
                        )),
                  ));
            }));
  }
}
