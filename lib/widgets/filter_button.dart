
import 'package:flutter/material.dart';
import 'package:rosterapp/widgets/filter_checkbox_menu.dart';

class FilterButton extends StatefulWidget {
  final Map<String, bool> initialValue;
  final Function(Map<String, bool>) onChange;
  final Function(Map<String, bool>) onSubmit;

  const FilterButton({
    Key key,
    @required this.onChange,
    @required this.initialValue,
    @required this.onSubmit,
  }) : super(key: key);

  @override
  _FilterButtonState createState() => _FilterButtonState();
}

class _FilterButtonState extends State<FilterButton> {
  final GlobalKey key = GlobalKey();
  final FocusNode focusNode = FocusNode();
  final LayerLink _layerLink = LayerLink();
  OverlayEntry overlayEntry;
  bool visible = false;
  bool isSelectedall;
  int selectedCount;

  @override
  void initState() {
    setState(() {
      isSelectedall = true;
      selectedCount = 0;
    });
    super.initState();
  }

  void setVisible(bool value) {
    setState(() {
      visible = value;
    });
  }

  void showOverlay(bool value) {
    if (value == true) {
      overlayEntry = _showOverlay();
      Overlay.of(key.currentContext).insert(overlayEntry);
    } else {
      if (overlayEntry != null) {
        overlayEntry.remove();
      }
    }
  }

  OverlayEntry _showOverlay() {
    final RenderBox box = key.currentContext.findRenderObject() as RenderBox;
    final Offset position = box.localToGlobal(Offset.zero);
    final Size size = box.size;

    return OverlayEntry(
      builder: (context) => Positioned(
        left: position.dx + size.width - 224,
        child: CompositedTransformFollower(
          link: _layerLink,
          showWhenUnlinked: false,
          offset: Offset(0.0, size.height + 5),
          child: Material(
            elevation: 4.0,
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              child: FilterCheckboxMenu(
                intialValue: widget.initialValue,
                onChange: (value, selectedAll, count) {
                  widget.onChange(value);
                  setState(() {
                    isSelectedall = selectedAll;
                    selectedCount = count;
                  });
                },
                onSubmit: (value) {
                  widget.onSubmit(value);
                  focusNode.unfocus();
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
      key: widget.key,
      link: _layerLink,
      child: InkWell(
        key: key,
        focusNode: focusNode,
        onFocusChange: (value) {
          if (focusNode.hasFocus) {
            setVisible(true);
            showOverlay(true);
          } else {
            setVisible(false);
            showOverlay(false);
          }
        },
        onTap: () {
          FocusScope.of(context).requestFocus(focusNode);

          if (!FocusScope.of(context).hasPrimaryFocus) {
            FocusScope.of(context).unfocus();
          }
        },
        child: Container(
          width: 250,
          padding: EdgeInsets.fromLTRB(18, 11, 16, 11),
          decoration: BoxDecoration(
            border: Border.all(color: Color(0xff9b9b9b), width: 0.1),
            borderRadius: BorderRadius.all(Radius.circular(4)),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Icon(
                    Icons.filter_list,
                    color: Color(0xff454955),
                    size: 14,
                  ),
                  SizedBox(
                    width: 7,
                  ),
                  Text(
                    'Filter by',
                    style: TextStyle(
                      fontFamily: 'Barlow-Regular',
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: Color(0xff575b61),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    isSelectedall ? 'All' : selectedCount.toString(),
                    style: TextStyle(
                      fontFamily: 'Barlow-Medium',
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: Color(0xff0ab0f1),
                    ),
                  ),
                  SizedBox(
                    width: 7,
                  ),
                  Icon(
                    Icons.arrow_drop_down,
                    color: Color(0xff0ab0f1),
                    size: 17,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
