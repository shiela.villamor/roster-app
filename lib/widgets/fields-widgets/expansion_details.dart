
import 'package:flutter/material.dart';
import 'package:rosterapp/models/student_model.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/profile-details/personal_details.dart';
import 'package:rosterapp/widgets/profile-details/contact_details.dart';
import 'package:rosterapp/widgets/profile-details/contact_person.dart';
import 'package:rosterapp/widgets/profile-details/enrolment_details.dart';

class ExpansionDetails extends StatefulWidget {
  final Student studentResult;

  ExpansionDetails(this.studentResult, {Key key})
      : super(key: key);

  @override
  _ExpansionDetailsState createState() => _ExpansionDetailsState();
}

class _ExpansionDetailsState extends State<ExpansionDetails> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xffdadada),
          borderRadius: BorderRadius.circular(5.0)
        ),
        child: Expanded(
          child: _buildExpansionDetails(widget.studentResult),
        )
      ),
    );
  }

  SingleChildScrollView _buildExpansionDetails(studentResult) {
    return SingleChildScrollView(
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: <Widget>[
          ExpansionTile(
            leading: Image.asset('assets/images/user.png', width: 30, height: 30),
            title: Text(
                    'Personal Details',
                    style: TextStyle(
                        color: Color(0xffA96E6E), fontFamily: 'Amaranth-Regular'
                    )
                ),
            children: <Widget>[
              PersonalDetails(studentResult)
            ],
          ),
          Divider(
            color: Colors.black,
          ),
          ExpansionTile(
            leading: Image.asset('assets/images/call.png', width: 30, height: 30),
            title: Text(
                'Contact Details',
                style: TextStyle(
                    color: Color(0xffA96E6E), fontFamily: 'Amaranth-Regular'
                )
            ),
            children: [
              ContactDetails(studentResult)
            ],
          ),
          Divider(
            color: Colors.black,
          ),
          ExpansionTile(
            leading: Image.asset('assets/images/contacts.png', width: 30, height: 30),
            title: Text(
                'Contact Person',
                style: TextStyle(
                    color: Color(0xffA96E6E), fontFamily: 'Amaranth-Regular'
                )
            ),
            children: [
              ContactPerson(studentResult)
            ],
          ),
          Divider(
            color: Colors.black,
          ),
          ExpansionTile(
            leading: Image.asset('assets/images/enrolment.png', width: 30, height: 30),
            title: Text(
                'Enrollment Details',
                style: TextStyle(
                    color: Color(0xffA96E6E), fontFamily: 'Amaranth-Regular'
                )
            ),
            children: [
              EnrolmentDetails(studentResult)
            ],
          )
        ],
      ),
    );
  }
}
