

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:rosterapp/providers/add_student_provider.dart';

class DatePickerButton extends ConsumerWidget {
  final String label;

  DatePickerButton({this.label});
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final submitProvider = watch(personalSubmitProvider);
    return InkWell(
      onTap: () {
        showDatePicker(
          context: context,
          initialDate: submitProvider.dateEnrolled ?? DateTime.now(),
          firstDate: DateTime(2014),
          lastDate: DateTime(2030),
        ).then((date) {
          if (date != null) {
            if (label == 'Date Enrolled') {
              if (submitProvider.dateEnrolled == null ||
                  date.isBefore(submitProvider.dateEnrolled)) {
                submitProvider.setDateEnrolled(date);
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Please enter a valid date.')));
              }
            } else {
              if (date.isAfter(submitProvider.dateEnrolled ?? DateTime.now())) {
                submitProvider.setDateEnrolled(date);
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Please enter a valid date.')));
              }
            }
          }
        });
      },
      child: Container(
        height: 55,
        margin: EdgeInsets.only(left: 24, right: 24, bottom: 12),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            border: label == 'Date Hired'
                ? Border.all(
                    color: submitProvider.dateEnrolled == null &&
                            submitProvider.isSubmitted
                        ? Color(0xffeb2121)
                        : Color(0xFF8E8E8E))
                : Border.all(color: Color(0xFF8E8E8E))),
        child: Padding(
          padding: const EdgeInsets.only(left: 12, right: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                  submitProvider.dateEnrolled == null
                        ? label
                        : DateFormat.yMMMMd('en_US')
                            .format(submitProvider.dateEnrolled),
                    style: submitProvider.dateEnrolled == null
                        ? TextStyle(
                        color: Color(0xFF8E8E8E),
                        fontSize: 11,
                        fontFamily: 'Amaranth-Regular'
                    ) : TextStyle(fontSize: 15,
                        fontFamily: 'Amaranth-Regular')),
              Icon(Icons.calendar_today, color: Color(0xffA96E6E), size: 18),
            ],
          ),
        ),
      ),
    );
  }
}

