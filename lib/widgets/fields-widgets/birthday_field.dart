/*

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:rosterapp/providers/add_student_provider.dart';

class BirthdayDatePickerButton extends ConsumerWidget {
  final String label;

  BirthdayDatePickerButton({this.label});
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final dateNotifier = watch(personalDateProvider);
    final submitProvider = watch(personalSubmitProvider);
    return InkWell(
      onTap: () {
        showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(1947),
          lastDate: DateTime.now(),
        ).then((date) {
          if (date != null) {
         //   dateNotifier.setBirthday(date);
            submitProvider.setBirthDate(date);
          } else {
            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Please enter a valid date.')));
          }
        });
      },
      child: Container(
        height: 55,
        margin: EdgeInsets.only(left: 24, right: 24, bottom: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(
              color: dateNotifier.birthday == null && submitProvider.isSubmitted
                  ? Color(0xffeb2121)
                  : Color(0xFF8E8E8E)),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 12, right: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                submitProvider.birthdate == null
                    ? label
                    : DateFormat.yMMMMd('en_US').format(submitProvider.birthdate),
                style: submitProvider.birthdate == null
            ? TextStyle(
                    color: Color(0xFF8E8E8E),
                  fontSize: 11,
                    fontFamily: 'Amaranth-Regular'
                ) :
                    TextStyle(
                      fontSize: 15,
                        fontFamily: 'Amaranth-Regular'
                    )
              ),
              Icon(Icons.calendar_today, color: Color(0xffA96E6E), size: 18),
            ],
          ),
        ),
      ),
    );
  }
}
*/
