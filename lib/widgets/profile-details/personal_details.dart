import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';
import 'package:rosterapp/widgets/custom_row.dart';


class PersonalDetails extends ConsumerWidget {
  final Student studentResult;

  PersonalDetails(this.studentResult, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRowLabel(
            label: 'First Name',
            input: studentResult.firstName,
          ),
          CustomRowLabel(
            label: 'Middle Name',
            input: studentResult.middleName,
          ),
          CustomRowLabel(
            label: 'Last Name',
            input: studentResult.lastName,
          ),
          CustomRowLabel(
            label: 'Birth Date',
            input: studentResult.birthdate,
          ),
          CustomRowLabel(
            label: 'Gender',
            input: studentResult.gender,
          ),
          CustomRowLabel(
            label: 'Address',
            input: studentResult.completeAddress,
          )
        ],
      ),
    );
  }
}
