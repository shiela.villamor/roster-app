import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';
import 'package:rosterapp/widgets/custom_row.dart';

class EnrolmentDetails extends ConsumerWidget {
  final Student studentResult;

  EnrolmentDetails(this.studentResult, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRowLabel(
            label: 'Student ID',
            input: studentResult.studentID,
          ),
          CustomRowLabel(
            label: 'Year',
            input: studentResult.year,
          ),
          CustomRowLabel(
            label: 'Section',
            input: studentResult.section,
          )
        ],
      ),
    );
  }
}
