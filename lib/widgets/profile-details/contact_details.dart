import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';
import 'package:rosterapp/widgets/custom_row.dart';

class ContactDetails extends ConsumerWidget {
  final Student studentResult;

  ContactDetails(this.studentResult, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRowLabel(
            label: 'Contact Number',
            input: studentResult.mobileNumber,
          ),
          CustomRowLabel(
            label: 'Alternate Contact Number',
            input: studentResult.alternateMobileNumber.toString().contains('null')
                    ? '-' : studentResult.alternateMobileNumber,
          ),
          CustomRowLabel(
            label: 'E-mail Address',
            input: studentResult.emailAdd,
          ),
          CustomRowLabel(
            label: 'Facebook',
            input: studentResult.facebookLink.toString().contains('')
                    ? '-' : studentResult.facebookLink,
          )
        ],
      ),
    );
  }
}
