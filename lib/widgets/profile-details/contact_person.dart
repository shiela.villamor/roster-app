import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';
import 'package:rosterapp/widgets/custom_row.dart';

class ContactPerson extends ConsumerWidget {
  final Student studentResult;

  ContactPerson(this.studentResult, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomRowLabel(
            label: 'Name',
            input: studentResult.contactName,
          ),
          CustomRowLabel(
            label: 'Relationship',
            input: studentResult.relationship,
          ),
          CustomRowLabel(
            label: 'Contact Number',
            input: studentResult.contactNumber,
          ),
        ],
      ),
    );
  }
}
