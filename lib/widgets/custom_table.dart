
import 'package:flutter/material.dart';
import 'package:rosterapp/helpers/constants.dart';
import 'package:rosterapp/models/models.dart';

class CustomTable extends StatelessWidget {
  final bool columnHeaderVisible;
  final String noDataFoundMessage;
  final List<TableColumn> columns;
  final List<TableRows> rows;
  final double dataRowHeight;
  final ScrollController scrollController;
  final bool loading;

  const CustomTable(
      {Key key,
      @required this.columns,
      @required this.rows,
      this.columnHeaderVisible = true,
      this.noDataFoundMessage,
      this.dataRowHeight = 67,
      this.scrollController,
      this.loading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          columnHeaderVisible
              ? _renderTableHeader(context)
              : Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xffececec),
                      ),
                    ),
                  ),
                ),
          if (loading == true)
            Expanded(child: Center(child: CircularProgressIndicator()))
          else
            rows.isEmpty
                ? _renderNoDataMessage(context)
                : Expanded(
                    child: SingleChildScrollView(
                      controller: scrollController,
                      child: Column(
                        children: [
                          Column(
                            children: [
                              ..._renderTableBody(context),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
        ],
      ),
    );
  }

  Widget _renderNoDataMessage(BuildContext context) {
    final isDesktop = MediaQuery.of(context).size.width > 900;

    return Expanded(
      child: Column(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: isDesktop
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.start,
              children: [
                Text(
                  noDataFoundMessage ?? 'No data found',
                  style: TextStyle(
                    fontSize: isDesktop ? 42 : 24,
                    color: primaryColor,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _renderTableHeader(BuildContext context) {
    return Container(
      height: 52,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Color(0xecececec)),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              SizedBox(width: 25),
              ...columns.map(
                (res) => Expanded(
                  flex: res.span ?? 1,
                  child: Text(
                    res.columnName,
                    style: TextStyle(
                      fontSize: 14,
                      fontFamily: 'Barlow-Regular',
                      color: Color(0xff9b9b9b),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  List<Widget> _renderTableBody(BuildContext context) {
    final isDesktop = MediaQuery.of(context).size.width > 900;

    return rows
        .map(
          (res) => Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => res.onPressed(),
              hoverColor: Color(0xff9b9b9b).withOpacity(0.08),
              child: Container(
                height: isDesktop ? 67 : 45,
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Color(0xffececec),
                    ),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(width: isDesktop ? 25 : 0),
                    ...res.cells.asMap().entries.map(
                          (entry) => Expanded(
                            flex: columns[entry.key].span ?? 1,
                            child: entry.value,
                          ),
                        ),
                  ],
                ),
              ),
            ),
          ),
        )
        .toList();
  }
}
