
import 'package:flutter/material.dart';
import 'package:rosterapp/widgets/custom_button.dart';

class FilterCheckboxMenu extends StatefulWidget {
  final Map<String, bool> intialValue;
  final Function(
    Map<String, bool>,
    bool isSelectedall,
    int selectedCount,
  ) onChange;
  final Function(Map<String, bool>) onSubmit;
  final bool isSelectedall;
  final int selectedCount;

  FilterCheckboxMenu({
    @required this.intialValue,
    this.onChange,
    this.onSubmit,
    this.isSelectedall,
    this.selectedCount,
  });

  @override
  FilterCheckboxMenuState createState() => FilterCheckboxMenuState();
}

class FilterCheckboxMenuState extends State<FilterCheckboxMenu> {
  final ScrollController _scrollController = ScrollController();
  List<String> selectedFilters = [];
  Map<String, bool> filterStatus = {};
  bool selectedAll = true;
  int totalCount = 0;
  int selectedCount = 0;

  void handleOnChange() {
    widget.onChange(filterStatus, selectedAll, selectedCount);
  }

  void onSubmit() {
    widget.onSubmit(filterStatus);
  }

  @override
  void initState() {
    filterStatus = widget.intialValue;
    filterStatus.forEach((key, value) {
      setState(() {
        totalCount++;
      });
      if (filterStatus[key] == true) {
        setState(() {
          selectedCount++;
        });
      }
    });
    if (totalCount != selectedCount) {
      setState(() {
        selectedAll = false;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.selected,
      };
      if (states.any(interactiveStates.contains)) {
        return Color(0xff0ab0f1);
      }
      return Colors.transparent;
    }

    return Container(
      width: 224,
      height: 259,
      decoration: BoxDecoration(
        color: Color(0xffffffff),
        boxShadow: [
          BoxShadow(
            color: Color(0xff474647).withOpacity(0.26),
            blurRadius: 12,
            offset: Offset(0, 6), //X, Y
          ),
        ],
      ),
      child: Column(
        children: [
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: SizedBox(
              height: 164,
              width: 212,
              child: Scrollbar(
                isAlwaysShown: true,
                controller: _scrollController,
                showTrackOnHover: false,
                radius: Radius.circular(2.5),
                thickness: 5,
                child: SizedBox(
                  width: 197,
                  height: 32,
                  child: Padding(
                    padding: EdgeInsets.only(right: 15),
                    child: ListView(
                      controller: _scrollController,
                      children: [
                        ListTile(
                            tileColor: Color(0xffffffff),
                            selectedTileColor: Color(0xfff7f7f7),
                            dense: true,
                            visualDensity:
                                VisualDensity(horizontal: -4, vertical: -4),
                            selected: selectedAll,
                            minLeadingWidth: 8,
                            horizontalTitleGap: 20,
                            minVerticalPadding: 0,
                            contentPadding: EdgeInsets.only(left: 21),
                            onTap: () {
                              setState(() {
                                selectedAll = !selectedAll;
                                if (!selectedAll) {
                                  selectedCount = 0;
                                }
                                filterStatus.forEach((key, value) {
                                  filterStatus[key] = selectedAll;
                                });
                              });
                              handleOnChange();
                            },
                            leading: ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3)),
                                child: Container(
                                  height: 15,
                                  width: 15,
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Color(0xffbcbcbc),
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(3)),
                                  ),
                                  child: Checkbox(
                                    fillColor:
                                        MaterialStateProperty.resolveWith(
                                            getColor),
                                    value: selectedAll,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedAll = value;
                                        filterStatus.forEach((key, v) {
                                          filterStatus[key] = value;
                                        });
                                      });
                                    },
                                  ),
                                )),
                            title: Text(
                              'All',
                              style: TextStyle(
                                color: selectedAll
                                    ? Color(0xff0ab0f1)
                                    : Color(0xff222a34),
                                fontSize: 14,
                                fontFamily: 'Barlow-Regular',
                              ),
                            )),
                        ...filterStatus.keys
                            .map((item) => ListTile(
                                tileColor: Color(0xffffffff),
                                selectedTileColor: selectedAll
                                    ? Color(0xffffffff)
                                    : Color(0xfff7f7f7),
                                dense: true,
                                visualDensity:
                                    VisualDensity(horizontal: -4, vertical: -4),
                                selected: filterStatus[item],
                                minLeadingWidth: 8,
                                horizontalTitleGap: 20,
                                minVerticalPadding: 0,
                                contentPadding: EdgeInsets.only(left: 21),
                                onTap: () {
                                  setState(() {
                                    filterStatus[item] = !filterStatus[item];
                                    selectedCount = 0;
                                  });
                                  filterStatus.forEach((key, value) {
                                    if (filterStatus[key] == true) {
                                      setState(() {
                                        selectedCount++;
                                      });
                                    }
                                  });
                                  if (selectedCount == totalCount) {
                                    setState(() {
                                      selectedAll = true;
                                    });
                                  } else {
                                    setState(() {
                                      selectedAll = false;
                                    });
                                  }
                                  handleOnChange();
                                },
                                leading: ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(3)),
                                    child: Container(
                                      height: 15,
                                      width: 15,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Color(0xffbcbcbc),
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(3)),
                                      ),
                                      child: Checkbox(
                                        fillColor:
                                            MaterialStateProperty.resolveWith(
                                                getColor),
                                        value: filterStatus[item],
                                        onChanged: (value) {
                                          setState(() {
                                            filterStatus[item] =
                                                !filterStatus[item];
                                            selectedCount = 0;
                                          });
                                          filterStatus.forEach((key, value) {
                                            if (filterStatus[key] == true) {
                                              setState(() {
                                                selectedCount++;
                                              });
                                            }
                                          });
                                          if (selectedCount == totalCount) {
                                            setState(() {
                                              selectedAll = true;
                                            });
                                          } else {
                                            setState(() {
                                              selectedAll = false;
                                            });
                                          }
                                        },
                                      ),
                                    )),
                                title: Text(
                                  item,
                                  style: TextStyle(
                                    color: filterStatus[item]
                                        ? (selectedAll
                                            ? Color(0xff222a34)
                                            : Color(0xff0ab0f1))
                                        : Color(0xff222a34),
                                    fontSize: 14,
                                    fontFamily: 'Barlow-Regular',
                                  ),
                                )))
                            .toList(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 17,
          ),
          SizedBox(
            width: 190.86,
            height: 26,
            child: CustomNonIconButton(
              radius: 2,
              text: 'Apply Filter',
              type: 'primary',
              variant: 'small',
              onPressed: () => onSubmit(),
              disable: false,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          SizedBox(
            width: 190.86,
            height: 26,
            child: CustomNonIconButton(
              radius: 2,
              text: 'Clear Filter',
              type: 'secondary',
              variant: 'small',
              onPressed: () {
                setState(() {
                  selectedAll = false;
                  filterStatus.forEach((key, value) {
                    filterStatus[key] = false;
                  });
                  selectedCount = 0;
                  selectedFilters.clear();
                });
                handleOnChange();
              },
              disable: false,
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
