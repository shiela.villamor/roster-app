import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

//For regular, mobile number, email text fields
// ignore: must_be_immutable
class CharFieldWidget extends ConsumerWidget {
  String input;
  String hint;
  bool required;
  bool enabled;
  Function(String) onFormSubmit;
  Function onChanged;
  TextInputType keyboardType;
  LengthLimitingTextInputFormatter textLimit;
  String initialValue;

  CharFieldWidget({
    this.hint,
    this.onFormSubmit,
    this.onChanged,
    this.initialValue,
    this.required = true,
    this.enabled = true,
    Key key,
  }) : super(key: key);

  bool isValidEmail(String value) {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(value);
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    if (hint == '09XXXXXXXXX') {
      keyboardType = TextInputType.number;
      textLimit = LengthLimitingTextInputFormatter(11);
    } else if (hint.contains('E-mail')) {
      keyboardType = TextInputType.emailAddress;
      textLimit = LengthLimitingTextInputFormatter(200);
    } else {
      textLimit = LengthLimitingTextInputFormatter(200);
    }

    return Container(
      margin: EdgeInsets.only(left: 24, right: 24, bottom: 12),
      child: TextFormField(
        inputFormatters: [textLimit],
        style: TextStyle(
          fontSize: 15.0, fontFamily: 'Amaranth-Regular'
        ),
        validator: (value) {
          if (hint == '09XXXXXXXXX') {
            if (value.isEmpty && required) {
              return 'Mobile Number is required';
            } else if (value.contains(RegExp('[a-zA-Z_@.#&+-,!?()]')) ||
                value.length != 11) {
              return 'Invalid Format';
            }
          } else if (hint.contains('E-mail')) {
            if (value.isEmpty) {
              return '$hint is required';
            } else if (!isValidEmail(value)) {
              return 'Invalid Format';
            }
          } else if (hint.contains('Address')) {
            if (value.isEmpty && required) {
              return '$hint is required';
            }
          } else if (hint.contains('Link')) {
            if (value.isEmpty && required) {
              return '$hint is required';
            } else if (value.contains(RegExp('[#&+,!?()]'))) {
              return 'Invalid Format';
            }
          } else {
            if (value.isEmpty && required) {
              return '$hint is required';
            } /*else if (value.contains(RegExp('[0-9_@.#&+-,!?()]'))) {
              return 'Invalid Format';
            }*/
          }

          input = value;
          onFormSubmit(input);
          return null;
        },
        keyboardType: keyboardType,
        onChanged: (text) => onChanged(text),
        initialValue: initialValue,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          enabled: enabled,
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: const Color(0xffA96E6E))),
          hintText: hint,
          hintStyle: TextStyle(
              color: const Color(0xFF8E8E8E),
              fontSize: 11, fontFamily: 'Amaranth-Regular'
              ),
        ),
      ),
    );
  }
}
