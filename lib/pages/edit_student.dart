/*

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';
import 'package:rosterapp/providers/edit_student_provider.dart';
import 'package:rosterapp/widgets/dropdown/edit_gender_dropdown.dart';
import 'package:rosterapp/widgets/dropdown/edit_section_dropdown.dart';
import 'package:rosterapp/widgets/dropdown/edit_year_dropdown.dart';
import 'package:rosterapp/widgets/text_field.dart';


class EditStudent extends StatefulWidget {
  final Student studentResult;

  EditStudent(this.studentResult, {Key key})
      : super(key: key);


  @override
  _EditStudentState createState() => _EditStudentState();
}

class _EditStudentState extends State<EditStudent> {
  final _formKey = GlobalKey<FormState>();
  bool isChecked = false;
  bool isEnabled = true;
  bool _isValid = false;
  bool isRequired = false;
  bool isVisible = true;

  final firstNameController = TextEditingController(text: 'Hello');
  final middleNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final mobileNumberController = TextEditingController();
  final alternateContactNumberController = TextEditingController();
  final emailAddController = TextEditingController();
  final facebookLinkController = TextEditingController();
  final addressController = TextEditingController();
  final contactNameController = TextEditingController();
  final relationshipController = TextEditingController();
  final contactNumberController = TextEditingController();
  final alternateMobileNumberController = TextEditingController();
  final studentIDController = TextEditingController();
  final imageController = TextEditingController();
  final birthdateController = TextEditingController();

  @override
  void initState() {
    firstNameController.text = 'Hello';
    middleNameController.text = widget.studentResult.middleName;
    lastNameController.text = widget.studentResult.lastName;
    mobileNumberController.text = widget.studentResult.mobileNumber;
    emailAddController.text = widget.studentResult.emailAdd;
    facebookLinkController.text = widget.studentResult.facebookLink;
    addressController.text = widget.studentResult.completeAddress;
    contactNameController.text = widget.studentResult.contactName;
    relationshipController.text = widget.studentResult.relationship;
    contactNumberController.text = widget.studentResult.contactNumber;
    alternateMobileNumberController.text = widget.studentResult.alternateMobileNumber;
    alternateContactNumberController.text = widget.studentResult.alternateContactNumber;
    studentIDController.text = widget.studentResult.studentID;
    birthdateController.text = widget.studentResult.birthdate;
    super.initState();
  }

  @override
  void dispose() {
    firstNameController.dispose();
    middleNameController.dispose();
    lastNameController.dispose();
    mobileNumberController.dispose();
    emailAddController.dispose();
    facebookLinkController.dispose();
    addressController.dispose();
    contactNameController.dispose();
    relationshipController.dispose();
    contactNumberController.dispose();
    alternateMobileNumberController.dispose();
    alternateContactNumberController.dispose();
    studentIDController.dispose();
    birthdateController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child){
      final editProvider = watch(editStudentProvider);
      final submitProvider = watch(personalSubmitProvider);

  */
/*    editProvider.firstName = widget.studentResult.firstName;
      editProvider.middleName = widget.studentResult.middleName;
      editProvider.lastName = widget.studentResult.lastName;
      editProvider.mobileNumber = widget.studentResult.mobileNumber;
      editProvider.emailAdd = widget.studentResult.emailAdd;
      editProvider.facebookLink= widget.studentResult.facebookLink;
      editProvider.completeAddress= widget.studentResult.completeAddress;
      editProvider.contactName = widget.studentResult.contactName;
      editProvider.relationship= widget.studentResult.relationship;
      editProvider.contactNumber = widget.studentResult.contactNumber;
      editProvider.alternateMobileNumber = widget.studentResult.alternateMobileNumber;
      editProvider.alternateContactNumber = widget.studentResult.alternateContactNumber;
      editProvider.studentID = widget.studentResult.studentID;
      editProvider.birthdate= widget.studentResult.birthdate;*//*


      return Scaffold(
        appBar: AppBar(
            elevation: 0,
            backgroundColor: Color(0xff903749),
            title: Text(
              'Edit Student',
              style: TextStyle(
                  fontSize: 15.0,
                  fontFamily: 'Amaranth-Regular'
              ),
            )
        ),
        body: Form(
          key: _formKey,
          onChanged: () {
            final provider = watch(personalSubmitProvider);
            if (provider.isSubmitted) {
              final isValid = _formKey.currentState.validate();
              if (_isValid != isValid) {
                setState(() {
                  _isValid = isValid;
                });
              }
            }
          },
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                pageTitle('Personal Details'),
                CharFieldWidget(widget.studentResult.image, 'Image URL', (value) {
                  editProvider.setImage(value);
                },
                    imageController, (value) =>
                        editProvider.setImage(value.toString())),
                */
/*TextFormField(initialValue: widget.studentResult.firstName,),
                TextFormField(initialValue: widget.studentResult.middleName, controller: middleNameController,),
                TextFormField(initialValue: ,)*//*

                CharFieldWidget(widget.studentResult.firstName, 'First Name', (value) {
                  editProvider.setFirstName(value);
                },
                    firstNameController,
                        (value) => editProvider.setFirstName(value.toString())),
                CharFieldWidget(widget.studentResult.middleName,'Middle Name', (value){
                  editProvider.setMiddleName(value);
                }, middleNameController, (value) => editProvider.setMiddleName(value)),
                CharFieldWidget(widget.studentResult.lastName, 'Last Name', (value){
                  editProvider.setLastName(value);
                }, lastNameController, (value) => editProvider.setLastName(value)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CharFieldWidget(widget.studentResult.birthdate, 'Birthday', (value) {
                      editProvider.setBirthDate(value);
                    }, birthdateController, (value) => editProvider.setBirthDate(value)),
                    // BirthdayDatePickerButton(label: 'Birthday'),
                    Consumer(builder: (context, watch, child) {
                      final dateNotifier = watch(personalDateProvider);
                      final provider = watch(personalSubmitProvider);
                      if(provider.isSubmitted && dateNotifier.birthday == null) {
                        return Container(
                          margin: EdgeInsets.only(left: 36, bottom: 10),
                          child: Text(
                              'Birthday is required.',
                              style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xffA96E6E),
                                  fontFamily: 'Amaranth-Regular'
                              )
                          ),
                        );
                      }
                      else{
                        return SizedBox();
                      }
                    },)
                  ],
                ),
                EditGenderDropdown(
                  dropdownProvider: editProvider,
                  submitProvider: submitProvider,
                ),
                CharFieldWidget(widget.studentResult.completeAddress, 'Address', (value) {
                  editProvider.setPresentLineAddress(value);
                }, addressController, (value) => editProvider.setPresentLineAddress(value.toString())),
                pageTitle('Contact Details'),
                CharFieldWidget(widget.studentResult.mobileNumber,'09XXXXXXXXX', (value) {
                  editProvider.setMobileNumber(value);
                }, mobileNumberController, (value) => editProvider.setMobileNumber(value.toString())),
                CheckboxListTile(
                  title: Text(
                    'Do you have an alternate contact number?',
                    style: TextStyle(
                        fontSize: 12,
                        color: const Color(0xff903749),
                        fontFamily: 'Amaranth-Regular'
                    ),
                  ),
                  value: isChecked,
                  onChanged: (value) {
                    setState(() {
                      isChecked = value;
                      isEnabled = !isChecked;
                      isRequired = isEnabled;
                      if(isChecked) {
                       // editProvider.setAlternateMobileNumber(alternateMobileNumber);
                      }
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,
                ),
                CharFieldWidget(widget.studentResult.alternateMobileNumber, '09XXXXXXXXX', (value) {
                  editProvider.setAlternateMobileNumber(value);
                }, alternateMobileNumberController, (value) =>
                    editProvider.setAlternateMobileNumber(value.toString()),
                    enabled: !isEnabled,
                    required: !isRequired
                ),
                CharFieldWidget(widget.studentResult.emailAdd,'E-mail Address', (value) {
                  editProvider.setPersonalEmail(value);
                }, emailAddController, (value) => editProvider.setPersonalEmail(value.toString())),
                CharFieldWidget(widget.studentResult.facebookLink, 'Facebook Link (optional)', (value) {
                  editProvider.setFacebookProfile(value);
                }, facebookLinkController, (value) => editProvider.setFacebookProfile(value.toString()),
                  required: false,),
                pageTitle('Contact Person (in case of emergency)'),
                CharFieldWidget(widget.studentResult.contactName, 'Emergency Contact Name', (value) {
                  editProvider.setContactName(value);
                }, contactNameController, (value) => editProvider.setContactName(value)),
                CharFieldWidget(widget.studentResult.relationship, 'Relationship', (value) {
                  editProvider.setRelationship(value);
                }, relationshipController, (value) => editProvider.setRelationship(value.toString())),
                CharFieldWidget(widget.studentResult.contactNumber,'09XXXXXXXXX', (value) {
                  editProvider.setContactNumber(value);
                }, contactNumberController,
                        (value) => editProvider.setContactNumber(value.toString())),
                pageTitle('Enrollment Details'),
                // DatePickerButton(label: 'Date Enrolled'),
                CharFieldWidget(widget.studentResult.studentID, 'Student ID' , (value){
                  editProvider.setStudentID(value);
                }, studentIDController,
                        (value) => editProvider.setStudentID(value.toString())),
                EditYearDropdown(
                  dropdownProvider: editProvider,
                  submitProvider: submitProvider,
                ),
                EditSectionDropdown(dropdownProvider: editProvider, submitProvider: submitProvider),
                Column(
                    children: [
                      Center(
                        child: SizedBox(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(10),
                              primary: Color(0xffA96E6E),
                            ),
                            child: Text(
                              'Proceed and Update Student',
                              style: TextStyle(
                                  fontFamily: 'Amaranth-Regular',
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16
                              ),
                            ),
                            onPressed: () {
                              editProvider.onSubmit(true);
                              if(_formKey.currentState.validate()){
                                editProvider.setId((editProvider.student.id));
                                editProvider.editStudent();
                                editProvider.resetValues();
                                editProvider.onSubmit(false);
                                Navigator.pop(context);
                              }
                            },
                          ),),
                      )

                    ]
                )
              ],
            ),
          ),
        ),
      );
    });

  }

  Container pageTitle(String title) {
    return Container(
      margin: EdgeInsets.only(left: 24, bottom: 15, top: 5),
      child: Text(title,
          style: TextStyle(
            color: const Color(0xff903749),
            fontSize: 14,
          )),
    );
  }

}
*/
