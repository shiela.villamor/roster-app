
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/providers/add_student_provider.dart';
import 'package:rosterapp/providers/list_student_provider.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/dropdown/gender_dropdown.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/dropdown/section_dropdown.dart';
import 'package:rosterapp/widgets/text_field.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/dropdown/year_dropdown.dart';

class AddStudent extends StatefulWidget {
  @override
  _AddStudentState createState() => _AddStudentState();
}

class _AddStudentState extends State<AddStudent> {
  final _formKey = GlobalKey<FormState>();

  String firstName;
  String middleName;
  String lastName;
  String birthDate;
  String gender;
  String mobileNumber;
  String alternateMobileNumber;
  String emailAdd;
  String facebookLink;
  String address;
  String contactName;
  String alternateContactNumber;
  String relationship;
  String contactNumber;
  String studentID;
  String image;

  String formTitle;

  bool isChecked = false;
  bool isEnabled = true;
  bool _isValid = false;
  bool isRequired = false;
  bool isVisible = true;

 /* final firstNameController = TextEditingController();
  final middleNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final mobileNumberController = TextEditingController();
  final alternateContactNumberController = TextEditingController();
  final emailAddController = TextEditingController();
  final facebookLinkController = TextEditingController();
  final addressController = TextEditingController();
  final contactNameController = TextEditingController();
  final relationshipController = TextEditingController();
  final contactNumberController = TextEditingController();
  final alternateMobileNumberController = TextEditingController();
  final studentIDController = TextEditingController();
  final imageController = TextEditingController();
  final birthdateController = TextEditingController();*/

  /*@override
  void dispose() {
    firstNameController.dispose();
    middleNameController.dispose();
    lastNameController.dispose();
    mobileNumberController.dispose();
    emailAddController.dispose();
    facebookLinkController.dispose();
    addressController.dispose();
    contactNameController.dispose();
    relationshipController.dispose();
    contactNumberController.dispose();
    alternateMobileNumberController.dispose();
    alternateContactNumberController.dispose();
    studentIDController.dispose();
    birthdateController.dispose();
    super.dispose();
  }*/

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child){
      final provider = watch(addStudentProvider);
      final submitProvider = watch(personalSubmitProvider);
      final listStudents = watch(listStudentProvider);
      final formTypeProvider = watch(addStudentProvider);

      if(formTypeProvider.formType == 'EDIT') {
        formTitle = 'Edit Student';
      }
      else{
        formTitle = 'Add Student';
      }

      return Scaffold(
        appBar: AppBar(
            elevation: 0,
            backgroundColor: Color(0xff903749),
            title: Text(
              formTitle,
              style: TextStyle(
                  fontSize: 15.0,
                fontFamily: 'Amaranth-Regular'
              ),
            )
        ),
        body: Form(
          key: _formKey,
          onChanged: () {
            final provider = watch(personalSubmitProvider);
            if (provider.isSubmitted) {
              final isValid = _formKey.currentState.validate();
              if (_isValid != isValid) {
                setState(() {
                  _isValid = isValid;
                });
              }
            }
          },
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                pageTitle('Personal Details'),
                CharFieldWidget(
                  hint: 'Image URL',
                  onFormSubmit: (value) {
                    image = value;
                    provider.setImage(image);
                  },
                  onChanged: (value) => provider.setImage(value.toString()),
                  initialValue: provider.image == 'null' ? '' : provider.image,
                ),
                CharFieldWidget(
                  hint: 'First Name',
                  onFormSubmit: (value) {
                    firstName = value;
                    provider.setFirstName(firstName);
                  },
                  onChanged: (value) => provider.setFirstName(value.toString()),
                  initialValue: provider.firstName == 'null' ? '' : provider.firstName,
                ),
                CharFieldWidget(
                  hint: 'Middle Name',
                  onFormSubmit: (value) {
                    middleName = value;
                    provider.setMiddleName(middleName);
                  },
                  onChanged: (value) => provider.setMiddleName(value.toString()),
                  initialValue: provider.middleName == 'null' ? '' : provider.middleName,
                ),
                CharFieldWidget(
                  hint: 'Last Name',
                  onFormSubmit: (value) {
                    lastName = value;
                    provider.setLastName(lastName);
                  },
                  onChanged: (value) => provider.setLastName(value.toString()),
                  initialValue: provider.lastName == 'null' ? '' : provider.lastName,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CharFieldWidget(
                      hint: 'Birthday',
                      onFormSubmit: (value) {
                        birthDate = value;
                        provider.setBirthDate(birthDate);
                      },
                      onChanged: (value) => provider.setBirthDate(value.toString()),
                      initialValue: provider.birthdate == 'null' ? '' : provider.birthdate,
                    ),
                   // BirthdayDatePickerButton(label: 'Birthday'),
                    Consumer(builder: (context, watch, child) {
                      final dateNotifier = watch(personalDateProvider);
                      final provider = watch(personalSubmitProvider);
                      if(provider.isSubmitted && dateNotifier.birthday == null) {
                        return Container(
                          margin: EdgeInsets.only(left: 36, bottom: 10),
                          child: Text(
                            'Birthday is required.',
                            style: TextStyle(
                              fontSize: 12,
                              color: Color(0xffA96E6E), 
                                fontFamily: 'Amaranth-Regular'
                            )
                          ),
                        );
                      }
                      else{
                        return SizedBox();
                      }
                    },)
                  ],
                ),
                genderDropdown(
                  dropdownProvider: provider,
                  submitProvider: submitProvider,
                ),
                CharFieldWidget(
                  hint: 'Complete Address',
                  onFormSubmit: (value) {
                    address = value;
                    provider.setPresentLineAddress(address);
                  },
                  onChanged: (value) => provider.setPresentLineAddress(value.toString()),
                  initialValue: provider.completeAddress == 'null' ? '' : provider.completeAddress,
                ),
                pageTitle('Contact Details'),
                CharFieldWidget(
                    hint: '09XXXXXXXXX',
                    onFormSubmit: (value) {
                      mobileNumber = value;
                      provider.setMobileNumber(mobileNumber);
                    },
                    onChanged: (value) =>
                        provider.setMobileNumber(mobileNumber),
                    initialValue: provider.mobileNumber == 'null'
                        ? ''
                        : provider.mobileNumber
                ),
                CheckboxListTile(
                  title: Text(
                    'Do you have an alternate contact number?',
                    style: TextStyle(
                      fontSize: 12,
                      color: const Color(0xff903749),
                        fontFamily: 'Amaranth-Regular'
                    ),
                  ),
                  value: isChecked,
                  onChanged: (value) {
                    setState(() {
                      isChecked = value;
                      isEnabled = !isChecked;
                      isRequired = isEnabled;
                      if(isChecked) {
                        provider.setAlternateMobileNumber(alternateMobileNumber);
                      }
                    });
                  },
                  controlAffinity: ListTileControlAffinity.leading,
                ),
                CharFieldWidget(hint: '09XXXXXXXXX',
                    onFormSubmit: (value) {
                      alternateMobileNumber = value;
                      provider.setAlternateMobileNumber(alternateMobileNumber);
                    },
                    onChanged: (value) {
                      if(value.toString().contains('null')){
                        provider.setAlternateMobileNumber('-');
                    }
                      else{
                        provider.setAlternateMobileNumber(alternateMobileNumber);
                      }
                    },
                    initialValue: provider.alternateMobileNumber == 'null'
                        ? ''
                        : provider.alternateMobileNumber,
                    enabled: !isEnabled,
                    required: !isRequired),
                CharFieldWidget(
                    hint: 'E-mail Address',
                    onFormSubmit: (value) {
                      emailAdd = value;
                      provider.setPersonalEmail(emailAdd);
                    },
                    onChanged: (value) =>
                        provider.setPersonalEmail(emailAdd),
                    initialValue: provider.emailAdd == 'null'
                        ? ''
                        : provider.emailAdd
                ),
                CharFieldWidget(
                  hint: 'Facebook Profile Link (optional)',
                  onFormSubmit: (value) {
                    facebookLink = value;
                    provider.setFacebookProfile(facebookLink);
                  },
                  onChanged: (value) {
                    if(value.toString().contains('null')){
                      provider.setFacebookProfile('-');
                    }
                    else{
                      provider.setFacebookProfile(facebookLink);
                    }
                  },
                  initialValue: provider.facebookLink == 'null'
                      ? ''
                      : provider.facebookLink,
                  required: false,
                ),
                pageTitle('Contact Person (in case of emergency)'),
                CharFieldWidget(
                  hint: 'Contact Name',
                  onFormSubmit: (value) {
                    contactName = value;
                    provider.setContactName(contactName);
                  },
                  onChanged: (value) =>
                      provider.setContactName(contactName),
                  initialValue: provider.contactName == 'null'
                      ? ''
                      : provider.contactName,
                ),
                CharFieldWidget(
                  hint: 'Relationship',
                  onFormSubmit: (value) {
                    relationship = value;
                    provider.setRelationship(relationship);
                  },
                  onChanged: (value) =>
                      provider.setRelationship(relationship),
                  initialValue: provider.relationship == 'null'
                      ? ''
                      : provider.relationship,
                ),
                CharFieldWidget(
                  hint: '09XXXXXXXXX',
                  onFormSubmit: (value) {
                    contactNumber = value;
                    provider.setContactNumber(contactNumber);
                  },
                  onChanged: (value) =>
                      provider.setContactNumber(contactNumber),
                  initialValue: provider.contactNumber == 'null'
                      ? ''
                      : provider.contactNumber,
                ),
                pageTitle('Enrollment Details'),
               // DatePickerButton(label: 'Date Enrolled'),
                CharFieldWidget(
                  hint: 'Student ID',
                  onFormSubmit: (value) {
                    studentID = value;
                    provider.setStudentID(studentID);
                  },
                  onChanged: (value) =>
                      provider.setStudentID(studentID),
                  initialValue: provider.studentID == 'null'
                      ? ''
                      : provider.studentID,
                ),
                YearDropdown(
                  dropdownProvider: provider,
                  submitProvider: submitProvider,
                ),
                SectionDropdown(dropdownProvider: provider, submitProvider: submitProvider),
                Column(
                  children: [
                    Center(
                      child: SizedBox(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.all(10),
                          primary: Color(0xffA96E6E),
                        ),
                        child: Text(
                          'Save',
                          style: TextStyle(
                              fontFamily: 'Amaranth-Regular',
                              fontWeight: FontWeight.w500,
                              fontSize: 16
                          ),
                        ),
                        onPressed: () {
                          provider.onSubmit(true);
                         if(_formKey.currentState.validate() && formTypeProvider.formType == 'EDIT'){
                           provider.setId(provider.id);
                           provider.editStudent();
                           provider.resetValues();
                           provider.onSubmit(false);
                           Navigator.pop(context);
                         }

                         else{
                           provider.setId((listStudents.listOfStudents.length + 1).toString());
                           provider.createStudent();
                           provider.resetValues();
                           provider.onSubmit(false);
                           Navigator.pop(context);
                         }
                        },
                      ),),
                    )

                  ]
                )
              ],
            ),
          ),
        ),
      );
    });

  }

  Container pageTitle(String title) {
    return Container(
      margin: EdgeInsets.only(left: 24, bottom: 15, top: 5),
      child: Text(title,
          style: TextStyle(
            color: const Color(0xff903749),
              fontSize: 14,
              )),
    );
  }

}
