import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rosterapp/models/tab_view_models.dart';
import 'package:rosterapp/widgets/custom_tab_view.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/page-widget/listpage_widget.dart';

import 'add_student.dart';

class HomeList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Color(0xffD55858),
      floatingActionButton: SizedBox(
          height: 50,
          width: 50,
          child: FloatingActionButton(
            backgroundColor: Color(0xff903749),
            onPressed: () {
              Navigator.push(context, CupertinoPageRoute(builder: (context) => AddStudent()));
            },
            child: Icon(
              Icons.add,
              size: 23.0,
            ),
          )
      ),
      body: CustomTabView(
        tabData: [
          TabViewModel(
              tabName: 'List of Students',
              widget: ListPageWidget()
          )
        ],
      ),
    );
  }
}
