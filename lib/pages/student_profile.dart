import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/models/student_model.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/page-widget/profile_widget.dart';


class StudentProfile extends ConsumerWidget {
  final Student studentResult;

  StudentProfile(this.studentResult, {Key key})
      : super(key: key);
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return Scaffold(appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xff903749),
      title: Text(
            'Profile',
            style: TextStyle(fontSize: 15),
      ),
      ),
        body: ProfileWidget(studentResult)
    );
  }
}



