
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rosterapp/pages/home_list.dart';
import 'package:rosterapp/providers/list_student_provider.dart';
import 'package:rosterapp/widgets/main_drawer.dart';
import 'file:///C:/Users/Shiela%20Mae%20Villamor/Desktop/rosterapp/rosterapp/lib/widgets/page-widget/schedule_widget.dart';

class HomePage extends StatefulWidget {
  final logoutAction;
  final String name;
  final String picture;
  final String givenName;
  final String email;

  HomePage(this.logoutAction, this.name, this.picture, this.givenName, this.email);


  @override
  _HomePageState createState() => _HomePageState(logoutAction, name, picture, givenName, email);
}

class _HomePageState extends State<HomePage> {
  final logoutAction;
  final String name;
  final String picture;
  final String givenName;
  final String email;

  _HomePageState(this.logoutAction, this.name, this.picture, this.givenName,
      this.email);

  int _selectedIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      final studentProvider = watch(listStudentProvider);
      studentProvider.getListOfStudents();
      return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Color(0xff903749),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                //mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '$name',
                        style: TextStyle(
                          fontSize: 14,
                          letterSpacing: 0.02,
                          fontWeight: FontWeight.w600, fontFamily: 'Amaranth-Regular',
                          color: Color(0xff330000),
                        ),
                      ),
                      Text(
                        '$email',
                        style: TextStyle(
                          fontSize: 11,
                          letterSpacing: 0.02,
                          fontWeight: FontWeight.w500, fontFamily: 'Amaranth-Regular',
                          color: Color(0xff330000),
                        ),
                      ),
                    ],
                  ),
                  PopupMenuButton(
                      icon: _renderAvatar(picture),
                      iconSize: 35,
                      offset: Offset(0, 50),
                      onSelected: (result) {
                        if (result == 'logout') {
                          logoutAction();
                        }
                      },
                      itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry>[
                        PopupMenuItem(
                            value: 'logout',
                            child: Text('Logout'))
                      ])
                ],
              )],
          )
          ,
        )
        ,
        drawer: MainDrawer(),
        body: SizedBox.expand(
          child: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: _pageController,
            onPageChanged: (index) {
              setState(() {
                _selectedIndex = index;
              });
            },
            children: <Widget>[
              HomeList(),
              SchedulePageWidget(studentProvider.listOfStudents),
            ],
          ),
        ),
        bottomNavigationBar: CurvedNavigationBar(
          index: _selectedIndex,
          color: Color(0xff903749),
          backgroundColor: Color(0xffe97878),
          height: 45,
          onTap: _onItemTapped,
          animationCurve: Curves.easeInOutExpo,
          items: <Widget>[
            Icon(Icons.library_books_outlined, size: 18, color: Colors.white),
            Icon(Icons.schedule_outlined, size: 18, color: Colors.white)
          ],
        ),
      );
    });

  }

  void _onItemTapped(int index){
    setState(() {
      _selectedIndex = index;
      _pageController.animateToPage(index,
          duration: new Duration(milliseconds: 900),
        curve: Curves.easeOutCubic
          );
    });
  }
}

  Widget _renderAvatar(String picture) {
    return CircleAvatar(
      backgroundColor: Color(0xff0ab0f1),
      backgroundImage: NetworkImage(picture ?? ''),
    );
  }
